FROM apache/airflow:2.7.2-python3.11

USER root
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
         golang-go wget nano vim xz-utils\
         nodejs npm\
         transmission-cli\
  && apt-get autoremove -yqq --purge \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

ADD "https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-linux64-gpl.tar.xz" .
RUN tar -xvf ffmpeg-master-latest-linux64-gpl.tar.xz \
  && mv ffmpeg-master-latest-linux64-gpl/bin/* /usr/bin/ \
  && rm ffmpeg-master-latest-linux64-gpl.tar.xz
RUN npm install --global yarn \
  && yarn global add tiktok-scraper

USER airflow
RUN python3 -m pip install --upgrade pip \
  && python3 -m pip install --upgrade pip setuptools wheel \
  && pip3 install yt-dlp==2023.10.13 \
  && python3 -m pip install bdfr --upgrade \
  && python3 -m pip install -U gallery-dl
  